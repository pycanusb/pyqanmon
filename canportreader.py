# -*- coding: utf-8 -*-
"""
Created on Thu Oct 28 10:13:21 2010

@author: Gysel
"""

from PyQt4 import QtCore
import pycanusb

class CanPortReader(QtCore.QThread):
    received = QtCore.pyqtSignal(pycanusb.CANMsg, name='canReceived')
    """ Description: A Thread which handles incoming and outgoing traffic. """
    def __init__(self, canPort = None, receiver = None, sampleRate = 100):
        super(CanPortReader, self).__init__()
        
        self._canPort = canPort
        self._recv = receiver

        self._readTimer = QtCore.QTimer()
        self._sampleRate = sampleRate #in Millisekunden         
        self._readTimer.timeout.connect(self.__canPortReader)        
        
        self._statTimer = QtCore.QTimer()
        self._statRate = sampleRate #in Millisekunden         
        self._statTimer.timeout.connect(self.__printStat)        
        
        self.stat = None
        
        if self._recv != None:
            self.received.connect(self._recv)

    def setCanPort(self, canPort):
        self._canPort = canPort
    def getCanPort(self):
        return self._canPort
        
    def setReciever(self, receiver):
        self._recv = receiver
    def getReceiver(self):
        return self._recv
                
    def startReading(self):
        if self.isRunning() == False:
            #if not self._canPort.isOpen():
            #    self._canPort.open()
            
            self.start()
            self._readTimer.start(self._sampleRate)
            self._statTimer.start(self._statRate)
            
    def stopReading(self):
        if self.isRunning() == True:
            self.exit()
            #self._serialPort.close()
            self._readTimer.stop()
            self.wait()

    
    def __canPortReader(self):
        """and self._serialPort.isOpen():"""
        if self._canPort != None:
            #msg = pycanusb.CANMsg()
            msg = self._canPort.read()
            if msg != None and msg != False:
                self.received.emit(msg)

    def __printStat(self):
        stat = self._canPort.getStatistics()
        if stat != self.stat:
            self.stat = stat
            print stat
        stat = self._canPort.status(True)
        if stat != '':
            print "Status: %s" % stat

    def run(self):
        returnCode = self.exec_()
        """
        Thread wird immer mit Fehler beendet. Fehlerhafter Returnwert.
        if returnCode != 0:
            DebugMessage().printOut("ComportIO().run(): Fehler beim beenden von Thread.", 1)
        """
        return returnCode
