 # -*- coding: utf-8 -*-
"""
This file is part of pyQanmon.

pyQanmon is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyQanmon is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyQanmon.  If not, see <http://www.gnu.org/licenses/>.

Copyright 2010, Martin Gysel
"""

import os,sys

# Import Qt modules
from PyQt4 import QtCore, QtGui, uic
import pycanusb
import canportreader
from msgview import *

MONITOR_COLUMN_CNT = 4

# Create a class for our main window
class Main(QtGui.QMainWindow):
    def __init__(self):
        super(Main, self).__init__()
        self.currentMonTab = 0
        self.currentMsgTab = 0
        
        self.ui = uic.loadUi("./main.ui")
        self.ui.actionAdd_Tab.triggered.connect(self.addMessageTab)
        
        self.ui.monitor_tw = QtGui.QTabWidget()
        self.ui.message_tw = QtGui.QTabWidget()
        
        #layout = QtGui.QVBoxLayout();
        #layout.addWidget(self.ui.monitor[0])
        
        self.ui.centralwidget.layout().addWidget(self.ui.monitor_tw)
        #self.ui.centralwidget.layout().addWidget(QtGui.QTabWidget())        
        
        # list of all tables in the monitor part
        self.ui.table_v = []        
        self.ui.msg_v = []        
        
        self.addMonitorTab(name="Monitor")
        self.addMessageTab()
        self.addTMessageTab()
        
        self.ui.dockWidget.setWidget(self.ui.message_tw)
        
        
        self.ui.show()
        
        self.connectAdapter()
        
        self.canReader = canportreader.CanPortReader(self.canusb, self.addPacket)
        self.canReader.startReading()

    def addTMessageTab(self, b=None, name=None):
        if name == None:
            name = "Message %d" % 5
        w = CANTableMsg()
        #w = CANMMsg()
        w.setSendRecv(self.sendMessage2)
        self.ui.message_tw.addTab(w, name)
        
    def addMessageTab(self, b=None, name=None):
        self.ui.msg_v.append(uic.loadUi("./message.ui"))
        if name == None:
            name = "Message %d" % len(self.ui.msg_v)
        self.ui.message_tw.addTab(self.ui.msg_v[-1], name)
        self.ui.msg_v[-1].sendButton.clicked.connect(self.sendMessage)
        self.ui.msg_v[-1].extId.setCheckState(QtCore.Qt.PartiallyChecked)
    
        
    def addMonitorTab(self, b=None, name=''):
        tab = self.ui.monitor_tw.addTab(QtGui.QWidget(), name)
        
        self.ui.table_v.append(QtGui.QTableWidget(1,MONITOR_COLUMN_CNT))
        h1 = QtGui.QTableWidgetItem("Timestamp")
        h2 = QtGui.QTableWidgetItem("CAN Id")
        h3 = QtGui.QTableWidgetItem("Length")
        h4 = QtGui.QTableWidgetItem("Message")
        self.ui.table_v[-1].removeRow(0)
        self.ui.table_v[-1].setHorizontalHeaderItem(0, h1)
        self.ui.table_v[-1].setHorizontalHeaderItem(1, h2)
        self.ui.table_v[-1].setHorizontalHeaderItem(2, h3)
        self.ui.table_v[-1].setHorizontalHeaderItem(3, h4)
        self.ui.table_v[-1].resizeColumnsToContents()
        
        self.ui.monitor_tw.widget(tab).setLayout(QtGui.QVBoxLayout())
        self.ui.monitor_tw.widget(tab).layout().addWidget(self.ui.table_v[-1])
        
        return tab

    def sendMessage(self):
        msg = pycanusb.CANMsg()
        if (self.ui.message_tw.currentWidget().formatId.currentText() == 'Hex'):
            msg.id = int(str(self.ui.message_tw.currentWidget().id.currentText()),16)
        elif (self.ui.message_tw.currentWidget().formatId.currentText() == 'Dec'):
            msg.id = int(self.ui.message_tw.currentWidget().id.currentText(),10)
        elif (self.ui.message_tw.currentWidget().formatId.currentText() == 'Bin'):
            msg.id = int(self.ui.message_tw.currentWidget().id.currentText(),2)
        
        msg.len = int(str(self.ui.message_tw.currentWidget().length.text()))
        
        dat = str(self.ui.message_tw.currentWidget().data.currentText())
        if (self.ui.message_tw.currentWidget().formatData.currentText() == 'Hex'):
            dat = dat[:msg.len*2]
            dat = dat.ljust(msg.len*2,'0')
            for i in range(msg.len):
                msg.data[i] = int(dat[:2],16)
                dat = dat[2:]
        elif (self.ui.message_tw.currentWidget().formatData.currentText() == 'Bin'):
            dat = dat[:msg.len*8]
            dat.ljust(msg.len*8,'0')
            for i in range(msg.len):
                msg.data[i] = int(dat[:8],2)
                dat = dat[8:]
        if self.ui.message_tw.currentWidget().extId.checkState() == QtCore.Qt.Checked:
            msg.flags |= pycanusb.CANMSG_EXTENDED
        elif self.ui.message_tw.currentWidget().extId.checkState() == QtCore.Qt.PartiallyChecked:
            if msg.id > 2047:
                msg.flags |= pycanusb.CANMSG_EXTENDED
        print self.canusb.write(msg)
        
    def sendMessage2(self):
        self.canusb.write(self.ui.message_tw.currentWidget().getMsg())

        
    def connectAdapter(self):
        self.canusb = pycanusb.CanUSB(bitrate='50')
        
    def addPacket(self, msg):
        print 'grr'
        row = self.ui.table_v[0].rowCount() 
        self.ui.table_v[0].insertRow(row)
        self.ui.table_v[0].setItem(row, 0, QtGui.QTableWidgetItem(str(msg.timestamp)))
        self.ui.table_v[0].setItem(row, 1, QtGui.QTableWidgetItem(str(msg.id)))
        self.ui.table_v[0].setItem(row, 2, QtGui.QTableWidgetItem(str(msg.len)))
        self.ui.table_v[0].setItem(row, 3, QtGui.QTableWidgetItem(msg.dataAsHexStr(msg.len)))
        self.ui.table_v[0].resizeColumnsToContents()
        
        
    def parseDataField(self, txt, form, length):
        txt = txt.strip()
        if form == 'Hex':
            if txt.find(',') != -1:
                # the bytes are divided by commas
                tlist = txt.split(',')
                while len(tlist) < length:
                    tlist.append('00')
            elif txt.find(' ') != -1:
                # the bytes are divided by spaces
                tlist = txt.split(' ')
                while len(tlist) < length:
                    tlist.append('00')
            else:
                txt = txt.ljust(2*length,'0')
                tlist = []
                for t in range(length):
                    tlist.append(txt[:2])
                    txt = txt[2:]
        elif form == 'Dec':
            pass
        elif form == 'Bin':
            pass
        return tlist

def main():
    # Again, this is boilerplate, it's going to be the same on
    # almost every app you write
    print "hallo"
    app = QtGui.QApplication(sys.argv)
    window = Main()
    # It's exec_ because exec is a reserved word in Python
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
